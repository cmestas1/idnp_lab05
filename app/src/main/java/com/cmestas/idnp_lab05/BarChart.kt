package com.cmestas.idnp_lab05

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.View

// TestBranch01

class BarChart (context: Context): View(context) {

    private lateinit var paint: Paint

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint = Paint()
        paint.setColor(Color.GREEN)
        var rect: Rect = Rect(20, 56, 200 ,112)
        canvas!!.drawRect(rect, paint)
    }
}