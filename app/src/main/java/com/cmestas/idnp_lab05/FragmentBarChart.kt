package com.cmestas.idnp_lab05

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment

class FragmentBarChart : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val barChart = BarChart(container!!.context)
        val view: View = inflater.inflate(R.layout.fragment_barchart, container, false)
        val relativeLayout: RelativeLayout = view.findViewById(R.id.draw)
        relativeLayout.addView(barChart)

        return view
    }
}